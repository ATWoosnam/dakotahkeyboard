//
//  KeyboardNumericSecond.swift
//  Keyboard Pls
//
//  Created by Liv  on 6/23/16.
//  Copyright © 2016 phillipsl. All rights reserved.
//

import UIKit

class KeyboardNumericSecond: UIView {
    var delegate: KeyboardDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    func initializeSubviews() {
        let xibFileName = "KeyboardNumericSecond"
        let view = NSBundle.mainBundle().loadNibNamed(xibFileName, owner: self, options: nil)[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    
    @IBAction func keyTapped(sender: UIButton) {
        self.delegate?.keyWasTapped(sender.titleLabel!.text!)
    }
    
    @IBAction func spaceTapped(sender: UIButton) {
        self.delegate?.keyWasTapped(" ")
    }
    
    @IBAction func backTapped(sender: UIButton) {
        self.delegate?.backTapped()
    }
    
    @IBAction func returnTapped(sender: UIButton) {
        self.delegate?.returnTapped()
    }
    
    @IBAction func abcTapped(sender: UIButton) {
        self.delegate?.uppercaseKeyboard()
    }
    
    @IBAction func numericTapped(sender: UIButton) {
        self.delegate?.numericKeyboard()
    }
}
