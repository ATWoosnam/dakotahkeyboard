//
//  KeyboardLowerAccented.swift
//  Keyboard Pls
//
//  Created by Liv  on 6/22/16.
//  Copyright © 2016 phillipsl. All rights reserved.
//


import UIKit

class KeyboardLowerAccented: UIView {
    var delegate: KeyboardDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    func initializeSubviews() {
        let xibFileName = "KeyboardLowerAccented"
        let view = NSBundle.mainBundle().loadNibNamed(xibFileName, owner: self, options: nil)[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    
    @IBAction func keyTapped(sender: UIButton) {
        self.delegate?.keyWasTapped(sender.titleLabel!.text!)
    }
    
    @IBAction func spaceTapped(sender: UIButton) {
        self.delegate?.keyWasTapped(" ")
    }
    
    @IBAction func backTapped(sender: UIButton) {
        self.delegate?.backTapped()
    }
    
    @IBAction func returnTapped(sender: UIButton) {
        self.delegate?.returnTapped()
    }
    
    @IBAction func accentTapped(sender: UIButton) {
        self.delegate?.lowercaseKeyboard()
    }
    
    @IBAction func uppercaseTapped(sender: UIButton) {
        self.delegate?.upperAccentKeyboard()
    }
    
    @IBAction func numericTapped(sender: UIButton) {
        self.delegate?.numericKeyboard()
    }

}
