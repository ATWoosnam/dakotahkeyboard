//
//  KeyboardGreyUpperAccented.swift
//  Keyboard Pls
//
//  Created by Andrew T Woosnam on 7/12/16.
//  Copyright © 2016 phillipsl. All rights reserved.
//

import UIKit

class KeyboardGreyUpperAccented: UIView {
    var delegate: KeyboardDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    func initializeSubviews() {
        let xibFileName = "KeyboardGreyUpperAccented"
        let view = NSBundle.mainBundle().loadNibNamed(xibFileName, owner: self, options: nil)[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    
    @IBAction func keyTapped(sender: UIButton) {
        self.delegate?.keyWasTapped(sender.titleLabel!.text!)
    }
    
    @IBAction func accentTapped(sender: UIButton) {
        self.delegate?.uppercaseKeyboard()
    }
    
    @IBAction func lowercaseTapped(sender: UIButton) {
        self.delegate?.lowerAccentKeyboard()
    }
    
}
