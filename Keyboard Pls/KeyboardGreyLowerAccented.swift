//
//  KeyboardGreyLowerAccented.swift
//  Keyboard Pls
//
//  Created by Andrew T Woosnam on 6/23/16.
//  Copyright © 2016 phillipsl. All rights reserved.
//

import UIKit

class KeyboardGreyLowerAccented: UIView {
    var delegate: KeyboardDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    func initializeSubviews() {
        let xibFileName = "KeyboardGreyLowerAccented"
        let view = NSBundle.mainBundle().loadNibNamed(xibFileName, owner: self, options: nil)[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    
    @IBAction func keyTapped(sender: UIButton) {
        self.delegate?.keyWasTapped(sender.titleLabel!.text!)
    }
    
    @IBAction func accentTapped(sender: UIButton) {
        self.delegate?.lowercaseKeyboard()
    }
    
    @IBAction func uppercaseTapped(sender: UIButton) {
        self.delegate?.upperAccentKeyboard()
    }

}
