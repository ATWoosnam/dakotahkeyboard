//
//  DataViewController.swift
//  Keyboard Pls
//
//  Created by Liv  on 6/21/16.
//  Copyright © 2016 phillipsl. All rights reserved.
//

import UIKit

class DataViewController: UIViewController, KeyboardDelegate {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var wordLabel: UILabel!
    
    var dataObject: String = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        
        let keyboardView = KeyboardUppercase(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
        keyboardView.delegate = self
        
        textField.inputView = keyboardView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.dataLabel!.text = dataObject
    }
    
    @IBAction func searchTapped(Sender: UIButton) {
        let word: String = textField.text!
        self.wordLabel!.text = word
        self.wordLabel.hidden = false
    }

  
    func keyWasTapped(character: String) {
            textField.insertText(character)
        
    }
    
    func backTapped() {
        let range = textField.selectedTextRange
        if range!.empty {
            
            let start = textField.positionFromPosition(range!.start, offset: -1);
            let end = textField.positionFromPosition(range!.end, offset: 0);
            if start != nil {
                textField.replaceRange(textField.textRangeFromPosition(start!, toPosition: end!)!, withText: "");
            }
        }
            
        else {
            textField.replaceRange(range!, withText: "");
        }
    }
    
    func returnTapped() {
        let word: String = textField.text!
        self.wordLabel!.text = word
        self.wordLabel.hidden = false
    }
    
    func lowercaseKeyboard() {
        let lowercaseView = KeyboardLowercase(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
      
        self.view.endEditing(true)
        lowercaseView.delegate = self
        
        textField.inputView = lowercaseView
        textField.becomeFirstResponder()
    }
    
    func uppercaseKeyboard() {
        let uppercaseView = KeyboardUppercase(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
        self.view.endEditing(true)
        uppercaseView.delegate = self
        
        textField.inputView = uppercaseView
        textField.becomeFirstResponder()
    }
    
    
    func lowerAccentKeyboard() {
    /* This block of code is responsible for the "flash accented keys" display switching from lowercase un-accented keys to lowercase accented keys.
    To disable this functionality just commment out everything between the two "=====" bars.
    */
//==============================================================
        let currentView = textField.inputView!
        if currentView.isKindOfClass(KeyboardLowercase) {
//        flash grey keys:
            let greyLowerAccentedView = KeyboardGreyLowerAccented(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
            
            self.view.endEditing(true)
            greyLowerAccentedView.delegate = self
            
            textField.inputView = greyLowerAccentedView
            textField.becomeFirstResponder()
            
            //  Include 0.5 second pause on the grey screen:
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
//                switch to non-grey keys:
                let lowerAccentedView = KeyboardLowerAccented(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
                
                self.view.endEditing(true)
                lowerAccentedView.delegate = self
                
                self.textField.inputView = lowerAccentedView
                self.textField.becomeFirstResponder()
            }
        } else {
//==============================================================
            let lowerAccentedView = KeyboardLowerAccented(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
            
            self.view.endEditing(true)
            lowerAccentedView.delegate = self
            
            self.textField.inputView = lowerAccentedView
            self.textField.becomeFirstResponder()
        }
    }
    
    
    func upperAccentKeyboard(){
    /* This block of code is responsible for the "flash accented keys" display when switching from lowercase un-accented keys to lowercase accented keys.
    To disable this functionality just commment out everything between the two "=====" bars.
    */
//============================================================== 
    let currentView = textField.inputView!
    if currentView.isKindOfClass(KeyboardUppercase) {
        //        flash grey keys:
        let greyLowerAccentedView = KeyboardGreyUpperAccented(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
        
        self.view.endEditing(true)
        greyLowerAccentedView.delegate = self
    
        textField.inputView = greyLowerAccentedView
        textField.becomeFirstResponder()
        
        //  Include 0.5 second pause on the grey screen:
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            //  switch to non-grey keys:

            let upperAccentedView = KeyboardUpperAccented(frame: CGRect(x: 0, y: 0, width: 0, height: 220))

            self.view.endEditing(true)
            upperAccentedView.delegate = self
            
            self.textField.inputView = upperAccentedView
            self.textField.becomeFirstResponder()
            }
        } else {
//==============================================================
        let upperAccentedView = KeyboardUpperAccented(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
            
            self.view.endEditing(true)
            upperAccentedView.delegate = self
            
            self.textField.inputView = upperAccentedView
            self.textField.becomeFirstResponder()
        } 
    }
    
    func numericKeyboard() {
        let numericView = KeyboardNumeric(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
        
        self.view.endEditing(true)
        numericView.delegate = self
        
        textField.inputView = numericView
        textField.becomeFirstResponder()
    }
    
    func numericSecondKeyboard() {
        let numericSecondView = KeyboardNumericSecond(frame: CGRect(x: 0, y: 0, width: 0, height: 220))
        
        self.view.endEditing(true)
        numericSecondView.delegate = self
        
        textField.inputView = numericSecondView
        textField.becomeFirstResponder()
    }
}